$.smartimagesImageEditor = {
    image: {},
    Layers: {
        layers: {},
        init: function (layers) {
            this.layers = layers;
            var layers_container = $('#image-layers-container');
            for(var id in this.layers) {
                var html = this.getLayerHtml(this.layers[id]);
                layers_container.append(html);
            }
        },
        getLayerHtml: function (layer) {
            var options = '';
            var html = '';
            if(layer.type = 'text') {
                options =  this.getLayerTextHtml(layer);
            } else if(layer.type = 'image') {
                options =  this.getLayerTextHtml(layer);
            } else if(layer.type = 'timer') {
                options =  this.getLayerTextHtml(layer);
            }
            if(options != '') {
                var base =  this.getLayerBaseHtml(layer);
                html = '<div class="image-layer">' +
                    '<div class="image-layer-main">'+base+'</div>' +
                    '<div class="image-layer-options">'+options+'</div>' +
                    '</div>';
            }
            return html;
        },
        getLayerBaseHtml: function (layer) {
            var name = 'Слой '+layer.id;
            if(layer.hasOwnProperty('name')) {
                name = layer.name;
            } else if(layer.hasOwnProperty('id')) {
                name = 'Слой '+layer.id;
            }
            var position = 0;
            if(layer.hasOwnProperty('position')) {
                position = layer.position;
            } else if(layer.hasOwnProperty('id')) {
                position = layer.id;
            }
            var id = layer.id;
            var html = '<div class="field">' +
                '<div class="name">Название слоя</div>' +
                '<div class="value"><input type="text" name="layer['+id+'][name]" value="'+name+'"></div>' +
                '</div>' +
                '<div class="field">' +
                '<div class="name">Позиция слоя</div>' +
                '<div class="value"><input type="number" name="layer['+id+'][position]" value="'+position+'"></div>' +
                '</div>'+
                '<div class="field">' +
                '<div class="name">позиция X</div>' +
                '<div class="value"><input type="number" name="layer['+id+'][x]" value="'+this.getLayerProperty(layer, "x","0")+'"></div>' +
                '</div>'+
                '<div class="field">' +
                '<div class="name">позиция Y</div>' +
                '<div class="value"><input type="number" name="layer['+id+'][y]" value="'+this.getLayerProperty(layer, "y","0")+'"></div>' +
                '</div>';
            return html;

        },
        getLayerImageHtml: function (layer)  {

        },
        getLayerTextHtml: function (layer) {
            var id = layer.id;
            var html = '<div class="field">' +
                '<div class="name">Текст</div>' +
                '<div class="value"><textarea name="layer['+id+'][content]">'+this.getLayerProperty(layer, "content","")+'</textarea></div>' +
                '</div>' +
                '<div class="field">' +
                '<div class="name">Размер</div>' +
                '<div class="value"><input name="layer['+id+'][size]" value="'+this.getLayerProperty(layer, "size","12")+'"></div>' +
                '</div>' +
                '<div class="field">' +
                '<div class="name">Цвет</div>' +
                '<div class="value"><input name="layer['+id+'][color]" value="'+this.getLayerProperty(layer, "color","#cccccc")+'"></div>' +
                '</div>';
            return html;
        },
        getLayerTimerHtml: function (layer) {
            var html = this.getLayerBaseHtml(layer);
        },
        getLayerProperty: function (layer, name, def) {
            def = def || '';
            if(layer.hasOwnProperty(name)) {
                return layer[name];
            } else {
                return def;
            }
        },
        binds: function() {

        }
    },
    init: function(image, layers) {
        this.Layers.init(layers);
        this.image = image;
    },
    binds: function() {
        this.Layers.binds();
    }
}; 