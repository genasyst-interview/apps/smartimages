<?php

/**
 * Class smartimagesImageLayerDispatcher
 */
class smartimagesImageLayerDispatcher
{

    /**
     * @var array
     */
    protected $listeners = array();

    /**
     * smartimagesImageLayerDispatcher constructor.
     */
    public function __construct()
    {
        $helper = smartimagesImageHelper::getInstance();
        $this->listeners = array(
            //'image' => new smartimagesImageImageLayerListener($helper),
            'text' => new smartimagesImageLayerListenerText($helper),
        );
    }

    /**
     * @param $name
     * @param $layer
     *
     * @throws Exception
     */
    public function event($name, $layer)
    {
        $listener = false;
        if ($layer instanceof smartimagesImageLayerImageInterface) {
            return;
        } else

            if ($layer instanceof smartimagesImageLayerText) {
                $listener = $this->listeners['text'];
            }
        if ($listener) {
            $listener->event($name, $layer);
        } else {
            throw new \Exception('LAYER NOT VALID');
        }

    }
}