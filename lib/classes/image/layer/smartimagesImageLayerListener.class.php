<?php

abstract class smartimagesImageLayerListener
{

    /**
     * @var smartimagesImageInfo
     */
    protected $info;

    /**
     * @var smartimagesImageHelper
     */
    protected $rh;

    /**
     * @param ImageInfo
     */
    public function __construct($helper)
    {
        $this->rh = $helper;
    }

    protected $events = array(
        'prepare' => 'prepareFrame',
        'finish'  => 'finishFrame',
    );

    public function event($name, $layer)
    {
        if (array_key_exists($name, $this->events)) {
            $method = $this->events[$name];
            if (method_exists($this, $method)) {
                $this->$method($layer);
            }
        }
    }
}