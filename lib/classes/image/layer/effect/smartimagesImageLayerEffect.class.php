<?php

/* EFFECTS */

/**
 * Class smartimagesImageLayerEffect
 */
abstract class smartimagesImageLayerEffect
{
    /**
     * @var null|smartimagesImageLayerPositionInterface
     */
    protected $layer = null;
    /**
     * @var array
     */
    protected $options = array();
    /**
     * @var null|smartimagesImageHelper
     */
    protected $rh = null;

    /**
     * smartimagesImageLayerEffect constructor.
     *
     * @param smartimagesImageLayerPositionInterface $layer
     * @param array                                  $options
     */
    public function __construct(smartimagesImageLayerPositionInterface $layer, $options = array())
    {
        $this->options = $options;
        $this->layer = $layer;
        $this->rh = smartimagesImageHelper::getInstance();
        $this->init();
    }

    protected function init()
    {
    }

    /**
     * @return null|smartimagesImageLayerPositionInterface
     */
    protected function getLayer()
    {
        return $this->layer;
    }

    /**
     * @return mixed
     */
    protected function getFrame()
    {
        return $this->getLayer()->getFrame();
    }

    /**
     *
     */
    public function before()
    {
    }

    /**
     *
     */
    public function after()
    {
    }
}