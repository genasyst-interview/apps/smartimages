<?php

/* EFFECTS */

/**
 * Class smartimagesImageLayerEffectPrint
 */
class smartimagesImageLayerEffectPrint extends smartimagesImageLayerEffectDinamic
{

    protected function init()
    {
        if (!array_key_exists('speed', $this->options)) {
            $this->options['speed'] = 10;
        }
        if (!array_key_exists('start', $this->options)) {
            $this->options['start'] = 0;
        }
    }

    public function before()
    {
        if ($this->getFrame() != null) {
            $text = $this->getLayer()->get('raw.text.label');
            $delay = round(100 / $this->options['speed'], 0, PHP_ROUND_HALF_UP);
            $time = $this->getFrame()->getStartTime();
            $count = 0;
            if ($time > $this->options['start']) {
                $current = $time - $this->options['start'];
                $count = round($current / $delay, 0, PHP_ROUND_HALF_UP);
            }
            if (mb_strlen($text) >= $count) {
                $this->getLayer()->set('text.label', mb_substr($text, 0, $count));
            } else {
                $this->getLayer()->set('text.label', $this->getLayer()->get('raw.text.label'));
            }
        }
    }
}