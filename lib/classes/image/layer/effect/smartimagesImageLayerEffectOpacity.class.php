<?php

/* EFFECTS */

/**
 * Class smartimagesImageLayerEffectOpacity
 */
class smartimagesImageLayerEffectOpacity extends smartimagesImageLayerEffectStatic
{
    /**
     * @var array
     */
    protected $points = array();
    /**
     * @var null
     */
    protected $resource = null;
    /**
     * @var int
     */
    protected $width = 1;

    /**
     * @throws Exception
     */
    protected function prepare()
    {
        // color
        if (!array_key_exists('rgb_color', $this->options)) {
            if (array_key_exists('hex_color', $this->options) && !array_key_exists('rgb_color', $this->options)) {
                $color = $this->rh->sanitizeHexColor($this->options['hex_color']);
            } else {
                $color = $this->rh->sanitizeHexColor('#000');
            }
            $this->options['rgb_color'] = $color['rgb'];
            $this->options['hex_color'] = $color['hex'];
        }
        $points = $this->rh->getTextPoints(
            $this->layer->get('text.font.filename'),
            $this->layer->get('text.font.size'),
            $this->layer->get('text.label'),
            $this->layer->get('text.line_spacing'),
            $this->layer->get('text.angle'),
            $this->layer->get('text.box.paddings')
        );
        $width = max(
            abs($points['x'][0] - $points['x'][2]),
            abs($points['x'][1] - $points['x'][3])
        );
        $height = max(
            abs($points['y'][0] - $points['y'][2]),
            abs($points['y'][1] - $points['y'][3])
        );

        if (intval($this->options['width']) > 0) {
            $this->width = intval($this->options['width']);
        }

        $resource = $this->rh->getEmptyGdResource($width + ($this->width * 2), $height + ($this->width * 2));
        $border_resource = $this->rh->getTextGdResource(
            $this->layer->get('text.font.filename'),
            $this->layer->get('text.font.size'),
            $this->options['rgb_color'],
            $this->layer->get('text.label'),
            $this->layer->get('text.line_spacing'),
            $this->layer->get('text.angle'),
            $this->layer->get('text.box.paddings'),
            $this->layer->get('text.box.rgb_color')
        );

        $this->points = array(
            1 => array(// смещение вверх
                'x' => $this->width,
                'y' => 0,
            ),
            2 => array(// смещение вниз
                'x' => $this->width,
                'y' => $this->width * 2,
            ),
            3 => array( // смещение вправо
                'x' => $this->width * 2,
                'y' => $this->width,
            ),
            4 => array(// смещение влево
                'x' => 0,
                'y' => $this->width,
            ),
            5 => array(// смещение вправо и вниз
                'x' => $this->width * 2,
                'y' => $this->width * 2,
            ),
            6 => array(//смещение вправо и вверх
                'x' => $this->width * 2,
                'y' => 0,
            ),
            7 => array( // смещение влево и вверх
                'x' => 0,
                'y' => 0,
            ),
            8 => array( // смещение влево и вниз
                'x' => 0,
                'y' => $this->width * 2,
            ),
        );
        foreach ($this->points as $point) {
            $resource = $this->rh->getMergedGdResource(
                $resource,
                $border_resource,
                $point['x'],
                $point['y'],
                'top_left'
            );
        }
        $this->resource = $resource;
    }

    /**
     *
     */
    public function after()
    {
        $this->layer->set('final.resource', $this->rh->getMergedGdResource(
            $this->resource,
            $this->layer->get('final.resource'),
            $this->width,
            $this->width,
            'top_left'
        ));
    }
}