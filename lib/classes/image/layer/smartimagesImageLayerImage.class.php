<?php

class smartimagesImageLayerImage extends smartimagesImageLayerImageAbstract implements
    smartimagesImageLayerPositionInterface, smartimagesImageLayerImageInterface
{

    /**
     * @inheritDoc
     */
    public function move($x, $y, $gravity = smartimagesImageLayerPositionInterface::MOVE_TOP_LEFT)
    {
        $this->set('regular.move.x', intval($x));
        $this->set('regular.move.y', intval($y));
        $this->set('regular.move.gravity', $gravity);

        return $this;
    }

}
