<?php


class smartimagesImageLayerText extends smartimagesImageLayerAbstract implements smartimagesImageLayerPositionInterface
{
    /* Обект для динамической подмены данных текста, размер, текст, положение */
    /* protected $get_object = null;*/
    /**
     * @inheritDoc
     */
    public function sanitizeHexColor($color)
    {
        if (!preg_match('/\\A#?([[:xdigit:]]{3}|[[:xdigit:]]{6})\\Z/', $color, $matches)) {
            throw new InvalidArgumentException(
                'invalid.hex.color."' . $color . '"."#CCC", "#F9F9F9"'
            );
        }

        $hex = strtoupper($matches[1]);
        if (3 === strlen($hex)) {
            $red = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $green = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $blue = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $red = hexdec(substr($hex, 0, 2));
            $green = hexdec(substr($hex, 2, 2));
            $blue = hexdec(substr($hex, 4, 2));
        }

        return ['hex' => '#' . $hex, 'rgb' => [$red, $green, $blue]];
    }

    public function font($filename, $size = 12, $color = '#FFF')
    {
        $this->set('text.font.filename', $filename);
        $this->set('text.font.size', $size);
        $this->set('text.font.hex_color', $color);

        return $this;
    }

    /*public function effect($obj) {
        $this->get_object = $obj;
    }*/
    public function get($name)
    {
        /* if(is_object($this->get_object) && $this->get_object->has($name)) {
             return $this->get_object->get($name);
         }*/
        if ($this->has($name)) {
            return $this->data[$name];
        }

        return null;

    }

    /**
     * @inheritDoc
     */
    public function label($label)
    {
        $this->set('text.label', $label);
        if (!$this->has('raw.text.label')) {
            $this->set('raw.text.label', $label);
        }


        return $this;
    }

    /**
     * @inheritDoc
     */
    public function angle($angle)
    {
        $this->set('text.angle', $angle);

        return $this;
    }

    public function lineSpacing($lineSpacing)
    {
        $this->set('text.line_spacing', $lineSpacing);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function box(array $paddings, $color = null)
    {
        $this->set('text.box.paddings', $paddings);
        $this->set('text.box.hex_color', $color);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function move($x, $y, $gravity = smartimagesImageLayerPositionInterface::MOVE_TOP_LEFT)
    {
        $this->set('regular.move.x', $x);
        $this->set('regular.move.y', $y);
        $this->set('regular.move.gravity', $gravity);

        return $this;
    }

    public function getResource()
    {
        if (!empty($this->getDinamicEffects())) {
            foreach ($this->getDinamicEffects() as $v) {
                $v->before();
            }
        }
        if (!empty($this->getStaticEffects())) {
            foreach ($this->getStaticEffects() as $v) {
                $v->before();
            }
        }

        if (!empty($this->get('text.label'))) {
            waLog::dump($this->get('raw.text.label'));
            $text_resource = $this->rh->getDinamicTextGdResource(
                $this->get('raw.text.label'),
                $this->get('text.font.filename'),
                $this->get('text.font.size'),
                $this->get('text.font.rgb_color'),
                $this->get('text.label'),
                $this->get('text.line_spacing'),
                $this->get('text.angle'),
                $this->get('text.box.paddings')
            );

        } else {
            $text_resource = $this->rh->getEmptyGdResource(1, 1);
        }

        $this->set('final.resource', $text_resource);
        if (!empty($this->getStaticEffects())) {
            foreach ($this->getStaticEffects() as $v) {
                $v->after();
            }
        }
        $resource = $this->rh->getTextGdPolygon(
            $this->get('text.font.filename'),
            $this->get('text.font.size'),
            $this->get('raw.text.label'),
            $this->get('text.line_spacing'),
            $this->get('text.angle'),
            $this->get('text.box.paddings'),
            $this->get('text.box.rgb_color')
        );
        $this->set('final.resource', $this->rh->getMergedGdResource(
            $resource,
            $this->get('final.resource'),
            0,
            0,
            smartimagesImageLayerPositionInterface::MOVE_TOP_LEFT
        ));


        return $this->get('final.resource');
    }
}
