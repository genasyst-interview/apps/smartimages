<?php

abstract class smartimagesImageLayerAbstract
{
    /*
     * @var smartimagesImageGifFrame| smartimagesImageFrame
     * */
    /**
     * @var null
     */
    protected $frame = null;
    /**
     * @var array
     */
    protected $data = array();
    /**
     * @var bool
     */
    protected $is_effect = false;
    /**
     * @var null|smartimagesImageHelper
     */
    protected $rh = null;
    /**
     * @var array
     */
    protected $register_effects = array(
        'border' => array('hex_color' => '#00cc00', 'width' => 2),
        'print'  => array('speed' => 10),
    );
    /**
     * @var array
     */
    protected $static_effects = array();
    /**
     * @var array
     */
    protected $dinamic_effects = array();

    /**
     * smartimagesImageLayerAbstract constructor.
     *
     * @param null $frame
     */
    public function __construct($frame = null)
    {
        $this->rh = smartimagesImageHelper::getInstance();
        $this->setFrame($frame);
    }

    /**
     * @return  smartimagesImageFrameGif
     */
    public function getFrame()
    {
        return $this->frame;
    }

    /**
     * @return bool
     */
    public function isEffect()
    {
        return $this->is_effect;
    }

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function get($name)
    {
        if ($this->has($name)) {
            return $this->data[$name];
        }

        return null;

    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $this->data);
    }

    /**
     * @param $name
     */
    public function remove($name)
    {
        if ($this->has($name)) {
            unset($this->data[$name]);
        }
    }

    /**
     * @return resource
     */
    abstract public function getResource();

    /**
     * @return smartimagesImageFrameInterface
     */
    public function done()
    {
        $frame = $this->frame;
        if ($frame instanceof smartimagesImageFrameInterface) {
            $frame->setLayer($this);

            return $frame;
        }
    }

    /**
     * @param $frame
     */
    public function setFrame($frame)
    {
        if ($frame instanceof smartimagesImageFrameInterface) {
            $this->frame = $frame;
        } else {

        }
    }

    /**
     * @param $resource
     *
     * @return resource
     */
    public function mergeGdResource($resource)
    {
        return $this->rh->getMergedGdResource(
            $resource,
            $this->getResource(),
            $this->get('regular.move.x'),
            $this->get('regular.move.y'),
            $this->get('regular.move.gravity')
        );
    }

    /**
     * @param       $effect_name
     * @param array $options
     *
     * @return $this
     */
    public function setEffect($effect_name, $options = array())
    {
        if (array_key_exists(strtolower($effect_name), $this->register_effects)) {
            $class_name = 'smartimagesImageLayerEffect' . ucfirst(strtolower($effect_name));
            if (class_exists($class_name)) {
                if (empty($options)) {
                    $options = $this->register_effects[strtolower($effect_name)];
                }
                $effect = new $class_name($this, $options);
                if ($effect instanceof smartimagesImageLayerEffectStatic) {
                    $this->static_effects[] = $effect;
                } elseif ($effect instanceof smartimagesImageLayerEffectDinamic) {
                    $this->dinamic_effects[] = $effect;
                }
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getStaticEffects()
    {
        return $this->static_effects;
    }

    /**
     * @return array
     */
    public function getDinamicEffects()
    {
        return $this->dinamic_effects;
    }
}