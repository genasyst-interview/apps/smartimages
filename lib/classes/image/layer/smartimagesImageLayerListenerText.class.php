<?php

class smartimagesImageLayerListenerText extends smartimagesImageLayerListener
{
    /**
     * @inheritDoc
     */
    public function prepareFrame(smartimagesImageLayerText $layer)
    {
        $this->processFont($layer);
        $this->processLabel($layer);
        $this->processAngle($layer);
        $this->processLineSpacing($layer);
        $this->processBox($layer);
    }

    public function sanitizeHexColor($color)
    {
        if (!preg_match('/\\A#?([[:xdigit:]]{3}|[[:xdigit:]]{6})\\Z/', $color, $matches)) {
            throw new InvalidArgumentException(
                'invalid.hex.color."' . $color . '"."#CCC", "#F9F9F9"'
            );
        }

        $hex = strtoupper($matches[1]);
        if (3 === strlen($hex)) {
            $red = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $green = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $blue = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $red = hexdec(substr($hex, 0, 2));
            $green = hexdec(substr($hex, 2, 2));
            $blue = hexdec(substr($hex, 4, 2));
        }

        return ['hex' => '#' . $hex, 'rgb' => [$red, $green, $blue]];
    }

    /**
     * @param  smartimagesImageLayerText $layer
     *
     * @throws BadMethodCallException
     */
    public function processFont(smartimagesImageLayerText $layer)
    {
        if (!$layer->has('text.font.filename')) {
            throw new BadMethodCallException('no.font.added');
        }

        $filename = (string)$layer->get('text.font.filename');
        $layer->set('text.font.filename', $filename);

        $size = (int)$layer->get('text.font.size');
        $size = (5 > $size) ? 5 : $size;
        $layer->set('text.font.size', $size);

        $color = (string)$layer->get('text.font.hex_color');
        $color = $this->sanitizeHexColor($color);
        $layer->set('text.font.hex_color', $color['hex']);
        $layer->set('text.font.rgb_color', $color['rgb']);
    }

    /**
     * @param smartimagesImageLayerText $layer
     */
    public function processLabel(smartimagesImageLayerText $layer)
    {
        if (!$layer->has('text.label')) {
            $label = '';
        } else {
            $label = (string)$layer->get('text.label');
        }
        $layer->set('text.label', $label);
    }

    /**
     * @param smartimagesImageLayerText $layer
     */
    public function processAngle(smartimagesImageLayerText $layer)
    {
        if (!$layer->has('text.angle')) {
            $angle = 0;
        } else {
            $angle = (int)$layer->get('text.angle');
        }
        $layer->set('text.angle', $angle);
    }

    /**
     * @param smartimagesImageLayerText $layer
     */
    public function processLineSpacing(smartimagesImageLayerText $layer)
    {
        if (!$layer->has('text.line_spacing')) {
            $lineSpacing = 0.5;
        } else {
            $lineSpacing = (float)$layer->get('text.line_spacing');
        }
        $layer->set('text.line_spacing', $lineSpacing);
    }

    /**
     * @param smartimagesImageLayerText $layer
     */
    public function processBox(smartimagesImageLayerText $layer)
    {
        if (!$layer->has('text.box.paddings')) {
            $layer->set('text.box.paddings', [0, 0, 0, 0]);
            $layer->set('text.box.hex_color', null);

            $layer->set('text.box.rgb_color', null);

            return;
        }

        $paddings = array_values($layer->get('text.box.paddings'));
        $arr = [];
        for ($i = 0; $i < 4; $i++) {
            if (!isset($paddings[$i]) || 0 > $paddings[$i]) {
                $arr[$i] = 0;
            } else {
                $arr[$i] = (int)$paddings[$i];
            }
        }
        $layer->set('text.box.paddings', $arr);

        if (null !== $color = $layer->get('text.box.hex_color')) {
            $color = $this->sanitizeHexColor($color);
            $layer->set('text.box.hex_color', $color['hex']);
            $layer->set('text.box.rgb_color', $color['rgb']);
        } else {
            $layer->set('text.box.rgb_color', null);
        }
    }

    //TODO: MODERATE, EFFECTS
    public function verifyFreeType(smartimagesImageLayerText $layer)
    {
        if (!$this->context->isFreeTypeSupported()) {
            throw new RuntimeException('adding.text.not.supported');
        }
    }
}