<?php


/**
 * Class smartimagesImageLayerDebug
 */
class smartimagesImageLayerDebug extends smartimagesImageLayerText implements smartimagesImageLayerPositionInterface
{
    /**
     * @inheritDoc
     */
    public function __construct($frame = null)
    {
        parent::__construct($frame);
        $this->set('text.font.filename', smartimagesImageHelper::getFont());
        $this->set('text.font.size', 10);
        $this->set('text.font.hex_color', '#000');
        $this->set('text.box.paddings', [3, 3, 3, 3]);
        $this->set('text.line_spacing', 1);
        $this->set('text.box.hex_color', '#fff');
        $color = (string)$this->get('text.font.hex_color');
        $color = $this->sanitizeHexColor($color);
        $this->set('text.font.hex_color', $color['hex']);
        $this->set('text.font.rgb_color', $color['rgb']);
    }

    /**
     * @param string $color
     *
     * @return $this
     */
    public function font($color = '#FFF')
    {
        $color = $this->sanitizeHexColor((string)$color);
        $this->set('text.font.hex_color', $color['hex']);
        $this->set('text.font.rgb_color', $color['rgb']);

        return $this;
    }

    /**
     * @param $name
     *
     * @return null
     */
    public function get($name)
    {
        if ($this->has($name)) {
            return $this->data[$name];
        }

        return null;

    }

    /**
     * @inheritDoc
     */
    public function label($label)
    {
        $this->set('text.label', $label);
        if (!$this->has('raw.text.label')) {
            $this->set('raw.text.label', $label);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function angle($angle)
    {
        $this->set('text.angle', $angle);

        return $this;
    }

    /**
     * @param $lineSpacing
     *
     * @return $this
     */
    public function lineSpacing($lineSpacing)
    {
        $this->set('text.line_spacing', $lineSpacing);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function box(array $paddings, $color = null)
    {
        $this->set('text.box.paddings', $paddings);
        $this->set('text.box.hex_color', $color);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function move($x, $y, $gravity = smartimagesImageLayerPositionInterface::MOVE_TOP_LEFT)
    {
        $this->set('regular.move.x', $x);
        $this->set('regular.move.y', $y);
        $this->set('regular.move.gravity', $gravity);

        return $this;
    }

    /**
     * @return null
     * @throws Exception
     */
    public function getResource()
    {
        $text = "IMAGE:"
            . "\n Общее время:" . $this->getFrame()->getImage()->getAllTime(true)
            . "\n Количество фреймов:" . $this->getFrame()->getImage()->getCountFrames()
            . "\nFRAME:"
            . "\n Позиция:" . $this->getFrame()->getPosition()
            . "\n Старт:" . $this->getFrame()->getStartTime(true)
            . "\n"
            . "\n";
        $text_resource = $this->rh->getTextGdResource(
            $this->get('text.font.filename'),
            $this->get('text.font.size'),
            $this->get('text.font.rgb_color'),
            $text,
            $this->get('text.line_spacing'),
            $this->get('text.angle'),
            $this->get('text.box.paddings')
        );
        $this->set('final.resource', $text_resource);


        return $this->get('final.resource');
    }
}
