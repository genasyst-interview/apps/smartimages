<?php

abstract class smartimagesImageLayerImageAbstract extends smartimagesImageLayerAbstract implements smartimagesImageLayerImageInterface
{
    protected $init_source = false;
    /*
     * @var smartimagesImageGifExtractedInterface
     * */
    protected $_extracted = null;

    protected $extracted_data = null;

    public function isSetImage()
    {
        return $this->init_source;
    }

    /**
     * @inheritDoc
     */
    public function filename($filename)
    {
        $this->set('image.filename', $filename);
        $this->init_source = true;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function extracted($extracted, $options = null)
    {
        if ($extracted instanceof smartimagesImageGifExtracted || $extracted instanceof smartimagesImageGifExtractedDelay) {
            $this->init_source = true;
            $this->_extracted = $extracted;
            $this->extracted_data = $options;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function contents($contents)
    {
        $this->set('image.contents', $contents);
        $this->init_source = true;

        return $this;
    }

    public function getImageFilePointer()
    {
        if ($this->init_source && !$this->has('image.fp')) {
            $fp = false;
            if (isset($this->_extracted) && $this->_extracted instanceof smartimagesImageGifExtracted) {
                $content = $this->_extracted->getImageContent((int)$this->extracted_data);
                $fp = fopen('php://temp', 'r+');
                fwrite($fp, $content);

            } elseif (isset($this->_extracted) && $this->_extracted instanceof smartimagesImageGifExtractedDelay) {
                if (!is_array($this->extracted_data) || !array_key_exists('time', $this->extracted_data)) {
                    $times = $this->frame->getTimes();
                    $time = array_shift($times);
                } else {
                    $time = (int)$this->extracted_data['time'];
                }
                if ((is_array($this->extracted_data) && array_key_exists('disposal', $this->extracted_data))) {
                    $content = $this->_extracted->getImageContent($time, (int)$this->extracted_data['disposal']);
                } else {
                    $content = $this->_extracted->getImageContent($time);
                }

                $fp = fopen('php://temp', 'r+');
                fwrite($fp, $content);

            } elseif ($this->has('image.contents')) {
                $fp = fopen('php://temp', 'r+');
                fwrite($fp, $this->has('image.contents'));
            } elseif ($this->has('image.http.url') || $this->has('image.filename')) {
                if (!$this->has('image.imc_uri')) {
                    $this->initImcUri();
                }
                if ($this->has('image.imc_uri')) {
                    $uri = $this->get('image.imc_uri');
                    $fp = fopen($uri, 'rb');
                }
            }
            $this->set('image.fp', $fp);

        }

        return $this->get('image.fp');
    }

    public function getImageInfo()
    {
        if (!$this->has('image.format')) {
            if (is_object($this->_extracted) && ($this->_extracted instanceof smartimagesImageGifExtractedInterface)) {
                $info = array(
                    'format' => smartimagesImageContext::FORMAT_GIF,
                    'width'  => $this->_extracted->getCanvasWidth(),
                    'height' => $this->_extracted->getCanvasHeight(),
                );
            } else {

                $info = smartimagesImageHelper::getImageInfo()
                    ->resolveFromFilePointer($this->getImageFilePointer());
            }
            $this->set('image.width', $info['width']);
            $this->set('image.height', $info['height']);
            $this->set('image.format', $info['format']);
        }

        return array(
            'width'  => $this->get('image.width'),
            'height' => $this->get('image.height'),
            'format' => $this->get('image.format'),
        );
    }

    /**
     * @inheritDoc
     */
    public function resize($width, $height, $option = smartimagesImageLayerImageInterface::RESIZE_SHRINK)
    {
        $this->set('image.resize.width', intval($width));
        $this->set('image.resize.height', intval($height));
        $this->set('image.resize.option', $option);

        return $this;
    }

    public function getResource()
    {
        $this->processMove();
        if ($this->has('final.resource')) {
            return $this->get('final.resource');
        }
        $info = $this->getImageInfo();
        $format = $info['format'];
        $resource = false;
        // Если контент из файла или по ссылке
        if ($this->has('image.http.url') || $this->has('image.filename')) {
            $this->initImcUri();
        }
        // Если контент из файла или по ссылке
        if ($this->has('image.imc_uri')) {
            $uri = $this->get('image.imc_uri');
            $resource = $this->rh->getGdResourceFromStream($format, $uri, true);
            // Если контент из текста
        } elseif ($this->has('image.contents')) {
            $contents = $this->get('image.contents');
            $resource = $this->rh->getGdResourceFromContents($format, $contents, true);
            // Если контент из объекта Extracted
        } elseif (isset($this->_extracted) && $this->_extracted instanceof smartimagesImageGifExtracted) {
            $content = $this->_extracted->getImageContent((int)$this->extracted_data);
            if (empty($content)) {
                throw new Exception('Ytn rjyntynf');
            }
            $this->set('image.contents', $content);
            $resource = $this->rh->getGdResourceFromContents('gif', $content, true);

        } elseif (isset($this->_extracted) && $this->_extracted instanceof smartimagesImageGifExtractedDelay) {

            if (!is_array($this->extracted_data) || !array_key_exists('time', $this->extracted_data)) {
                $times = $this->frame->getTimes();
                $time = array_shift($times);
            } else {
                $time = (int)$this->extracted_data['time'];
            }
            if ((is_array($this->extracted_data) && array_key_exists('disposal', $this->extracted_data))) {
                $resource = $this->_extracted->getImageResource($time, (int)$this->extracted_data['disposal']);
            } else {
                $resource = $this->_extracted->getImageResource($time);
            }
            $frame = $this->_extracted->getFrameByTime($time);
            if ($frame['linked_disposal'] == 0) {
                $this->getFrame()->offDisposal();
            }

        }

        if ($resource && $this->has('image.resize.width')) {
            $res = $this->rh->getClonedGdResource($resource);
            $resource = $this->rh->getResizedGdResource(
                $res,
                $this->get('image.resize.width'),
                $this->get('image.resize.height'),
                $this->get('image.resize.option'),
                true
            );
        }
        /*ob_start();
        imagegif($resource);
        $thisff = ob_get_contents();
        ob_end_clean();
        echo    '<img src="data:image/gif;base64,' . base64_encode($thisff) . '" />';*/
        $this->set('final.resource', $resource);

        return $this->get('final.resource');
    }

    public function initImcUri()
    {
        smartimagesImcStream::register();
        $arr = false;
        if ($this->has('image.http.url')) {
            $arr = [
                'uri'        => $this->get('image.http.url'),
                'data_limit' => $this->get('image.http.data_limit'),
                'timeout'    => $this->get('image.http.timeout'),
                'seek'       => true,
                'global'     => true,
            ];
        } elseif ($this->has('image.filename')) {
            $arr = [
                'uri'  => $this->get('image.filename'),
                'seek' => true,
            ];
        }

        if ($arr) {
            $uri = 'imc://' . serialize($arr);
            $this->set('image.imc_uri', $uri);
        }

    }

    public function initFinalDimensions()
    {
        $width = $this->get('image.width');
        $height = $this->get('image.height');
        if ($this->has('image.resize.width')) {
            $args = $this->rh->getResizeArguments(
                $width,
                $height,
                $this->get('image.resize.width'),
                $this->get('image.resize.height'),
                $this->get('image.resize.option')
            );
            if ($args) {
                $width = $args['dst_w'];
                $height = $args['dst_h'];
            }
        }

        $this->set('final.width', $width);
        $this->set('final.height', $height);
    }

    public function processMove()
    {
        if (!$this->has('regular.move.x')) {
            $this->set('regular.move.x', 0);
            $this->set('regular.move.y', 0);
            $this->set('regular.move.gravity', smartimagesImageLayerPositionInterface::MOVE_CENTER);

            return;
        }

        $x = (int)$this->get('regular.move.x');
        $this->set('regular.move.x', $x);

        $y = (int)$this->get('regular.move.y');
        $this->set('regular.move.y', $y);

        $gravity = (string)$this->get('regular.move.gravity');

        $this->set('regular.move.gravity', $gravity);
    }

    /**
     * @param smartimagesImageImageLayer
     */
    public function termFilePointer(smartimagesImageLayerImageAbstract $layer)
    {
        if ($this->has('image.fp')) {
            fclose($this->get('image.fp'));
            $this->remove('image.fp');
        }
    }

    /**
     * @param smartimagesImageImageLayer
     */
    public function termImcUri(smartimagesImageLayerImageAbstract $layer)
    {

        if ($this->has('image.imc_uri')) {
            smartimagesImcStream::fclose($this->get('image.imc_uri'));
            $this->remove('image.imc_uri');
        }

    }
}