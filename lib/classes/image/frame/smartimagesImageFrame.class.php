<?php

class smartimagesImageFrame implements smartimagesImageFrameInterface
{
    /**
     * @var smartimagesImage|smartimagesImageGif
     */
    protected $image = null;

    /**
     * @var array
     */
    protected $data = array();

    /**
     * @var array
     */
    protected $layers = array();

    public function __construct(smartimagesImage $image, $data = array())
    {
        $this->image = $image;
    }

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $this->data);
    }

    /**
     * @param $name
     */
    public function remove($name)
    {
        if ($this->has($name)) {
            unset($this->data[$name]);
        }
    }

    /**
     * @return smartimagesImageGif
     * @api
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return smartimagesImageLayerBackground
     * @api
     */
    public function addBackgroundLayer()
    {
        return $this->layers[0] = new smartimagesImageLayerBackground($this);
    }

    /**
     * @return smartimagesImageLayerImage
     * @api
     */
    public function addImageLayer()
    {
        return new smartimagesImageLayerImage($this);
    }

    /**
     * @return smartimagesImageLayerText
     * @api
     */
    public function addTextLayer()
    {
        return new smartimagesImageLayerText($this);
    }

    /**
     * @param smartimagesImageLayerAbstract $layer
     * @param bool                          $position
     *
     * @return $this
     */
    public function setLayer(smartimagesImageLayerAbstract $layer, $position = false)
    {
        if ($position !== false) {
            $this->layers[(int)$position] = $layer;
        } else {
            $this->layers[] = $layer;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getLayers()
    {
        return $this->layers;
    }

    /**
     * @return mixed
     */
    public function getBackgroundLayer()
    {
        return $this->layers[0];
    }

    /**
     * @return int
     */
    public function getCanvasWidth()
    {
        if (!empty($this->image->getCanvasWidth())) {
            return $this->image->getCanvasWidth();
        } else {
            $this->setSize();

            return $this->image->getCanvasWidth();
        }
    }

    /**
     * @return int
     */
    public function getCanvasHeight()
    {
        if (!empty($this->image->getCanvasHeight())) {
            return $this->image->getCanvasHeight();
        } else {
            $this->setSize();

            return $this->image->getCanvasHeight();
        }
    }

    /**
     *
     */
    protected function setSize()
    {
        if (!empty($this->layers)) {
            $width = $height = 0;
            foreach ($this->layers as $layer) {
                if ($layer instanceof smartimagesImageLayerImageInterface) {
                    if ($layer->isSetImage()) {
                        $info = $layer->getImageInfo();
                        if ($info && !empty($info['width']) && $info['width'] > $width) {
                            $width = $info['width'];
                            $height = $info['height'];
                        }
                    }
                }
            }
            $this->image->setCanvasWidth($width);
            $this->image->setCanvasHeight($height);
        }
    }

    /**
     *
     */
    public function render()
    {

    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        if ($this->has('gif.frame_contents')) {
            return $this->get('gif.frame_contents');
        }
        $content = $this->getImage()->getFrameBuilder()->createImage($this);

        return $content;
    }

}