<?php

interface  smartimagesImageFrameInterface
{
    /**
     * @return smartimagesImageLayerBackground
     */
    public function addBackgroundLayer();

    /**
     * @return smartimagesImageLayerImage
     */
    public function addImageLayer();

    /**
     * @return smartimagesImageLayerText
     */
    public function addTextLayer();

    /**
     * @param smartimagesImageLayerAbstract $layer
     * @param bool                          $position
     *
     * @return $this
     */
    public function setLayer(smartimagesImageLayerAbstract $layer, $position = false);

    /**
     * @return array
     */
    public function getLayers();

    /**
     * @return smartimagesImageLayerBackground
     */
    public function getBackgroundLayer();

    /**
     * @return integer
     */
    public function getCanvasWidth();

    /**
     * @return integer
     */
    public function getCanvasHeight();

    /**
     * @return smartimagesImage
     */
    public function getImage();

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return mixed
     */
    public function set($name, $value);

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get($name);

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function has($name);

    /**
     * @param  string $name
     *
     * @return mixed
     */
    public function remove($name);

}