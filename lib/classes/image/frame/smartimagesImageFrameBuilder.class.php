<?php

/* FRAME BUILDER */

class smartimagesImageFrameBuilder
{
    protected $helper = null;
    protected $extractor = null;
    protected $optimizer = null;

    public function __construct()
    {
        $this->helper = smartimagesImageHelper::getInstance();
        $this->extractor = new smartimagesImageGifExtractor();
        $this->optimizer = new smartimagesImageGifOptimizer();
    }


    public function createImage(smartimagesImageFrameInterface $frame)
    {
        $this->initContents($frame);
        if ($frame->has('gif.frame_contents')) {
            return $frame->get('gif.frame_contents');
        }
    }

    /**
     * @param smartimagesImageFrameInterface $frame
     */
    protected function initContents($frame)
    {
        $this->initQuality($frame);
        $this->initCacheResource($frame);
        $this->initFrameContents($frame);
    }

    /**
     * @param $frame
     */
    protected function initQuality(smartimagesImageFrameInterface $frame)
    {
        if ($frame->getImage()->isInitQuality()) {
            return;
        }
        $count_frames = 1;
        if ($frame instanceof smartimagesImageFrameGif) {
            $count_frames = $frame->getImage()->getCountFrames();
        }
        $width = $frame->getCanvasWidth();
        $height = $frame->getCanvasHeight();
        $pixels = $count_frames * $width * $height;
        if ((250000 > $width * $height) && 2400000 > $pixels) {
            $quality = true;
        } else {
            $quality = false;
        }
        $frame->getImage()->setQuality($quality);
    }

    /**
     * @param smartimagesImageFrameInterface $frame
     */
    protected function initCacheResource(smartimagesImageFrameInterface $frame)
    {
        $layers = $frame->getLayers();
        $dispatcher = new smartimagesImageLayerDispatcher();
        $resource = $this->helper->getEmptyGdResource($frame->getCanvasWidth(), $frame->getCanvasHeight());
        $frame->set('gif.cache_resource', $resource);
        foreach ($layers as $layer) {
            $dispatcher->event('prepare', $layer);
            if ($layer instanceof smartimagesImageLayerBackground) {
                continue;
            }
            $layer->setFrame($frame);
            $resource = $layer->mergeGdResource($frame->get('gif.cache_resource'));

            if (!$frame->getImage()->getQuality() && imageistruecolor($resource)) {
                //$resource = $this->rh->getPalettizedGdResource($resource);
            }
            $frame->set('gif.cache_resource', $resource);
        }
    }

    /**
     * @param smartimagesImageFrameInterface $frame
     */
    protected function initFrameContents(smartimagesImageFrameInterface $frame)
    {
        $quality = $frame->getImage()->getQuality();
        $dm = $frame->getDisposal();
        $layer = $frame->getBackgroundLayer();
        $frame_resource = $this->helper->getEmptyGdResource($frame->getCanvasWidth(), $frame->getCanvasHeight());

        $frame_resource = $this->helper->getMergedGdResource(
            $frame_resource,
            $this->helper->getClonedGdResource($layer->getResource())
        );
        $resource = $this->helper->getMergedGdResource(
            $frame_resource,
            $frame->get('gif.cache_resource')
        );
        if (!$quality) {
            $contents = $this->getGifContentsFromGdResource($resource);
            imagedestroy($resource);
            $frame->set('gif.frame_contents', $contents);

            return;
        }
        $frame->set('gif.cache_resource', $resource);
        if ($frame->getPosition() > 0) {
            $prev_frame = $frame->getImage()->getFrame(($frame->getPosition() - 1));
            $controlResource = $prev_frame->get('gif.cache_resource');
            $resource = $this->optimizer->getOptimizedGdResource($resource, $controlResource);
            if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_PREVIOUS !== $dm) {
                imagedestroy($controlResource);
            }
        }

        $contents = $this->getGifContentsFromGdResource($resource);
        $frame->set('gif.frame_contents', $contents);
    }

    /**
     * @param  resource $resource
     *
     * @return string
     */
    protected function getGifContentsFromGdResource($resource)
    {
        return $this->helper->getContentsFromGdResource(smartimagesImageContext::FORMAT_GIF, $resource, [], true);
    }

    /**
     * @param  string $contents
     *
     * @return resource
     */
    protected function getGifResourceFromContents($contents)
    {
        return $this->helper->getGdResourceFromContents(smartimagesImageContext::FORMAT_GIF, $contents, true);
    }
}