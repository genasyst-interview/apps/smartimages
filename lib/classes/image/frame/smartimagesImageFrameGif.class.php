<?php

class smartimagesImageFrameGif extends smartimagesImageFrame
{
    protected $delay = 100;
    protected $output_format = 'gif';
    protected $position = 0;
    protected $disposal = 1;

    public function __construct(smartimagesImageGif $image, $data = array())
    {
        parent::__construct($image, $data);
        $this->position = $image->getCurrent();
    }

    public function addBackgroundLayer($layer = null)
    {
        if ($layer instanceof smartimagesImageLayerBackground) {
            $this->layers[0] = $layer;

            return $this;
        } else {
            return $this->layers[0] = new smartimagesImageLayerBackground($this);
        }
    }

    /**
     * @return smartimagesImageLayerImage
     * @api
     */
    public function addImageLayer($layer = null)
    {
        if ($layer instanceof smartimagesImageLayerAbstract) {
            $this->setLayer($layer);

            return $this;
        } else {
            return new smartimagesImageLayerImage($this);
        }
    }

    /**
     * @return smartimagesImageLayerText
     * @api
     */
    public function addTextLayer($layer = null)
    {
        if ($layer instanceof smartimagesImageLayerAbstract) {
            $this->setLayer($layer);

            return $this;
        } else {
            return new smartimagesImageLayerText($this);
        }
    }

    public function setDelay($delay = 100)
    {

        if (intval($delay) > 0) {

            $this->delay = (int)$delay;
            $this->image->setFrameDelay($this->position, $this->delay);
        }
    }

    public function getTimes()
    {
        return $this->image->getFrameTimes($this->position);
    }

    public function getStartTime($sec = false)
    {
        $times = $this->getTimes();
        $time = array_shift($times);

        return ($sec) ? $time / 100 : $time;
    }

    public function getDelay()
    {
        return $this->delay;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function offDisposal()
    {
        $this->disposal = 0;
    }

    public function onDisposal()
    {
        $this->disposal = 1;
    }

    public function setDisposal($disposal = 1)
    {
        $this->disposal = $disposal;
    }

    public function getDisposal()
    {
        return $this->disposal;
    }
}