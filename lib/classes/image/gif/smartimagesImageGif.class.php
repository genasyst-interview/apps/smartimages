<?php

class     smartimagesImageGif extends smartimagesImage
{
    const TYPE = 'gif';

    protected $loops = 0;

    public $frames_delay = array();
    public $frames_time = array();
    public $times_frame = array();
    public $current_frame = 0;
    public $all_time = 0;


    protected $frames = array();

    public function setLoops($loops)
    {
        $this->loops = (int)$loops;
    }

    protected function buildImage($type)
    {

        /*  $this->render();*/
        $builder = new smartimagesImageGifBuilderPlus();
        $builder->loop($this->loops);
        $builder->setResponseType($type);

        ob_end_clean();
        foreach ($this->frames as $v) {
            $builder->addFrameFromFrame($v);
        }

        if ($type == self::STREAM_TYPE_BUFFER) {
            return $builder->getContents();
        } else {
            $builder->addFooter();
        }

        return true;
    }

    public function getFrameBuilder()
    {
        if ($this->frame_builder == null) {
            $this->frame_builder = new smartimagesImageFrameBuilder();
        }

        return $this->frame_builder;
    }

    /**
     * @param array $data
     *
     * @return smartimagesImageFrame|smartimagesImageFrameGif
     */
    public function addFrame($data = array())
    {
        $this->current_frame = count($this->frames);
        $this->frames[$this->current_frame] = new smartimagesImageFrameGif($this, $data);

        return $this->frames[$this->current_frame];
    }

    public function setFrameDelay($frame_number = 0, $delay)
    {
        $this->frames_delay[(int)$frame_number] = $delay;
        $this->recalcTime();
    }

    protected function recalcTime()
    {
        $this->all_time = 0;
        $this->frames_time = array();
        foreach ($this->frames_delay as $id => $time) {
            $this->all_time += $time;
            $start = count($this->frames_time);

            $this->frames_time = array_pad($this->frames_time, $this->all_time, $id);
            $time_frame = array_slice($this->frames_time, $start, $time, true);
            $this->times_frame[$id] = array_keys($time_frame);
        }
    }

    public function getAllTime($sec = true)
    {
        return ($sec) ? $this->all_time / 100 : $this->all_time;

    }

    public function getFrameTimes($frame_position = 0)
    {
        if (array_key_exists($frame_position, $this->times_frame)) {
            return $this->times_frame[$frame_position];
        }

        return array(0);
    }

    public function getCurrent()
    {
        return $this->current_frame;
    }

    public function getCurrentFrame()
    {
        return $this->frames[$this->current_frame];
    }

    public function getFrame($frame_position = 0)
    {
        if (array_key_exists($frame_position, $this->frames)) {
            return $this->frames[$frame_position = 0];
        }

        return false;
    }

    public function getCountFrames()
    {
        return count((array)$this->frames);
    }

    public function unsetFrame($position)
    {
        if (array_key_exists($position, $this->frames)) {
            unset($this->frames[$position]);
        }
    }

    public function getFramesByTime($start = 0, $end = 100)
    {

    }

    protected function render()
    {
        return $this->frames;
    }
}