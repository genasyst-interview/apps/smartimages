<?php


class smartimagesImageGifExtractedDelay implements smartimagesImageGifExtractedInterface
{
    protected $extracted = null;
    protected $loop = null;
    protected $time_map = array();
    protected $all_time = 0;
    protected $helper = null;

    protected $debug = false;

    protected $frames = array();
    protected $background_frames = array();
    protected $cache_resource = array();
    protected $disposal = 2;
    protected $init_frames = array();

    public function __construct(smartimagesImageGifExtracted $extracted)
    {
        $this->helper = smartimagesImageHelper::getInstance();
        $this->extracted = $extracted;
        $this->optimizer = new smartimagesImageGifOptimizer();
        $this->init();
    }

    public function setloop($flag = false)
    {
        $this->loop = (bool)$flag;
    }

    public function getTimeMap()
    {
        return $this->time_map;
    }

    public function getAllTime()
    {
        return $this->all_time;
    }

    public function getCanvasWidth()
    {
        return $this->extracted->getCanvasWidth();
    }

    public function getCanvasHeight()
    {
        return $this->extracted->getCanvasHeight();
    }

    public function hasNetscapeExtension()
    {
        return $this->extracted->hasNetscapeExtension();
    }

    public function getGlobalColorTableFlag()
    {
        return $this->extracted->getGlobalColorTableFlag();
    }

    public function getTotalGlobalColors()
    {
        return $this->extracted->getTotalGlobalColors();
    }

    public function getTotalLoops()
    {
        if ($this->loop == null) {
            // return $this->extracted->getTotalLoops();
        }

        return $this->loop;
    }

    public function isAnimated()
    {
        return $this->extracted->isAnimated();
    }

    public function count()
    {
        return count($this->frames);
    }

    public function getImageContent($delay = 0, $disposal = 1)
    {
        $frame = $this->getFrameIndex($delay);
        if (array_key_exists($frame, $this->frames)) {
            return $this->getImageContentFromResource($this->getImageResource($delay, $disposal));
        }
        throw new \Exception('Нет такогшо кадра');
    }

    public function getImageResource($delay = 0, $disposal = 1)
    {
        $frame = $this->getFrameIndex($delay);
        if (array_key_exists($frame, $this->frames)) {
            $frame = $this->getFrame($frame);
            if ($frame['linked_disposal'] == 1 && $disposal == 1 && $frame['id'] > 0) {
                $frame_background = $this->getFrame($this->getSection($frame['id']));
                $image_resource = $this->helper->getClonedGdResource($frame['image_resource']);
                $back_resource = $this->helper->getClonedGdResource($frame_background['image_resource']);
                $nn = $this->helper->getMergedGdResource($back_resource, $image_resource);

                return $nn;
            } else {
                return $frame['image_resource'];
            }
        }
        throw new \Exception('Нет такогшо кадра' . $delay . ' ');
    }

    public function getFrameResource($delay = 0)
    {
        $frame = $this->getFrameIndex($delay);
        if (array_key_exists($frame, $this->frames)) {
            $frame = $this->getFrame($frame);

            return $frame['frame_resource'];
        }
        throw new \Exception('Нет такогшо кадра' . $delay . ' ');
    }

    public function getBase64Content($delay = 0)
    {
        return base64_encode($this->getImageContent($delay));
    }

    public function getBase64Image($delay = 0)
    {
        return '<img src="data:image/gif;base64,' . $this->getBase64Content($delay) . '" />';
    }

    public function getBase64FrameImage($delay = 0)
    {
        $frame = $this->getFrameIndex($delay);
        if (array_key_exists($frame, $this->frames))
            $this->prepareFrames($frame);
        {
            return '<img src="data:image/gif;base64,' . base64_encode($this->getImageContentFromResource($this->frames[$frame]['frame_resource'])) . '" />';
        }

        return '<img src="" />';
    }

    public function getImageResourceFromId($frame = 0)
    {

        if (array_key_exists($frame, $this->frames)) {
            $frame = $this->getFrame($frame);
            $frame_background = $this->getFrame($this->getSection($frame));
            $frame_background['image_resource'];

            return $this->helper->getMergedGdResource($frame_background['frame_resource'], $frame['image_resource']);
        }
        throw new \Exception('Нет такогшо кадра');
    }

    public function getFrame($id = 0)
    {
        try {
            if (array_key_exists($id, $this->frames)) {
                $this->prepareFrames($id);

                return $this->frames[$id];
            }
        } catch (Exception $e) {
            echo $e->getTraceAsString();
            exit;
        }

        throw new \Exception('Нет такогшо кадра');
    }

    public function getFrameByTime($delay = 0)
    {
        $frame = $this->getFrameIndex($delay);
        if (array_key_exists($frame, $this->frames)) {
            $this->prepareFrames($frame);

            return $this->frames[$frame];
        }
        throw new \Exception('Нет такогшо кадра');
    }

    public function getFrameIndex($delay = 0)
    {
        if ($delay >= $this->all_time) {
            if ($this->loop) {
                $loop = $delay / $this->all_time;
                $delay_float = abs(intval($loop) - $loop);

                return $this->time_map[intval(round($this->all_time * $delay_float, 0))];
            } else {
                $keys = array_keys($this->time_map);
                $last_key = array_pop($keys);

                return $this->time_map[$last_key];
            }
        } else {
            return $this->time_map[$delay];
        }
    }

    protected function prepareFrames($i)
    {
        $section = $this->getSection($i);
        if (array_key_exists($section, $this->init_frames) && array_key_exists($i, $this->init_frames[$section])) {
            return;
        }
        if ($i == $section) {
            $this->initFrame($this->frames[$i], $section);

            return;
        }
        $max = 0;
        if (array_key_exists($section, $this->init_frames) && !empty($this->init_frames[$section])) {
            $max = max($this->init_frames[$section]);
        }

        $max = max(array($max, $section));
        $slice = array_slice($this->frames, $max, ($i + 1) - $max);
        foreach ($slice as $frame) {
            $this->initFrame($frame, $section);
        }

    }

    protected function initFrame($frame, $section)
    {
        if (array_key_exists($frame['id'], $this->init_frames)) {
            return;
        }
        $frame_resource = $this->helper->getGdResourceFromContents('gif', $frame['frame_image']);
        $this->frames[$frame['id']]['frame_resource'] = $frame_resource;
        $frame_resource = $this->helper->getClonedGdResource($frame_resource);

        if ($frame['id'] == $section) {
            $newResource = $this->helper->getEmptyGdResource($this->extracted->getCanvasWidth(), $this->extracted->getCanvasHeight());
        } else {
            if (!array_key_exists($section, $this->cache_resource)) {
                $newResource = $this->helper->getEmptyGdResource($this->extracted->getCanvasWidth(), $this->extracted->getCanvasHeight());
            } else {
                $newResource = $this->cache_resource[$section];
            }

            if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_PREVIOUS === $frame['disposal']) {
                $newResource = $this->helper->getClonedGdResource($newResource);
            }
        }
        $newResource = $this->helper->getMergedGdResource($newResource, $frame_resource, $frame['left'], $frame['top']);
        //imagedestroy($frame_resource);
        $this->cache_resource[$section] = $newResource;
        if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_NONE === $frame['disposal']) {

            $newResource = $this->helper->getClonedGdResource($newResource);
        }

        $this->frames[$frame['id']]['image_resource'] = $newResource;
        $this->init_frames[$section][$frame['id']] = $frame['id'];
        /*$ewResource = $this->rh->getEmptyGdResource($this->extracted->getCanvasWidth(), $this->extracted->getCanvasHeight());
        $frame_resource = $this->rh->getMergedGdResource($ewResource, $frame_resource,  $frame['left'],  $frame['top']);
        if($frame['id'] == $section)  {
            $this->frames[$frame['id']]['image_resource'] = $this->rh->getClonedGdResource($frame_resource);
            $this->init_frames[$section][$frame['id']] = $frame['id'];
        } else {

            if(!array_key_exists($section,  $this->cache_resource)) {
                $newResource = $this->rh->getEmptyGdResource($this->extracted->getCanvasWidth(), $this->extracted->getCanvasHeight());
            } else {
                $newResource =  $this->frames[$frame['id']-1]['image_resource'];
            }
            if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_PREVIOUS ===  $frame['disposal']) {
                $newResource = $this->rh->getClonedGdResource($newResource);
            }

            // $frame_resource = $this->optimizer->getOptimizedGdResource($frame_resource, $newResource);
            $newResource = $this->rh->getMergedGdResource($newResource,  $frame_resource, 0, 0);




            if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_NONE === $frame['disposal']) {
                $this->cache_resource[$section] = $newResource;
                $newResource = $this->rh->getClonedGdResource($newResource);
            }*/
        /* if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_NONE !==  $frame['linked_disposal'] || $frame['id']==$section) {
             $newResource = $this->rh->getEmptyGdResource($this->extracted->getCanvasWidth(), $this->extracted->getCanvasHeight());
         } else {
             $newResource = $this->cache_resource[$section];
 
             if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_PREVIOUS === $frame['disposal']) {
                 $newResource = $this->rh->getClonedGdResource($newResource);
             }
             $newResource = $this->rh->getClonedGdResource($newResource);
         }
        $newResource = $this->rh->getMergedGdResource($newResource, $frame_resource, $frame['left'],  $frame['top']);
        // imagedestroy($frame_resource);
         $this->cache_resource[$section] = $newResource;
         if (smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_NONE === $frame['disposal']) {
             $this->cache_resource[$section] = $newResource;
             $newResource = $this->rh->getClonedGdResource($newResource);
         }
 
 
 
             $this->frames[$frame['id']]['image_resource'] = $newResource;
             $this->init_frames[$section][$frame['id']] = $frame['id'];*/


    }

    public function getCountFrames()
    {
        return count($this->frames);
    }

    protected function getSection($i)
    {
        $temp = 0;
        if (array_key_exists($i, $this->background_frames)) {
            return $i;
        }
        foreach ($this->background_frames as $k => $v) {
            if ($k >= $i) {
                return $temp;
            }
            $temp = $k;
        }

        return $temp;
    }

    protected function init()
    {
        $start = microtime(true);
        for ($i = 0, $this->extracted->seek(0); $this->extracted->valid(); $this->extracted->seek(++$i)) {
            $delay = $this->extracted->getDelayTime();
            $this->all_time = $this->all_time + $delay;
            $this->time_map = array_pad($this->time_map, $this->all_time, $i);
            $builder = new smartimagesImageGifBuilder();

            $builder
                ->imageWidth($this->extracted->getImageWidth())
                ->imageHeight($this->extracted->getImageHeight())
                ->colorTable($this->extracted->getColorTable())
                ->interlaceFlag($this->extracted->getInterlaceFlag())
                ->imageData($this->extracted->getImageData());
            if ($this->extracted->getTransparentColorFlag()) {
                $index = $this->extracted->getTransparentColorIndex();
                $builder->transparentColorIndex($index);
            }
            $this->frames[$i]['id'] = $i;
            $this->frames[$i]['width'] = $this->extracted->getImageWidth();
            $this->frames[$i]['height'] = $this->extracted->getImageHeight();
            $this->frames[$i]['top'] = $this->extracted->getImageTop();
            $this->frames[$i]['left'] = $this->extracted->getImageLeft();
            $this->frames[$i]['delay'] = $this->extracted->getDelayTime();
            $this->frames[$i]['disposal'] = $this->extracted->getDisposalMethod();
            $this->frames[$i]['linked_disposal'] = $this->extracted->getLinkedDisposalMethod();
            $this->frames[$i]['interlace'] = $this->extracted->getInterlaceFlag();
            $this->frames[$i]['color_table'] = $this->extracted->getColorTable();
            $this->frames[$i]['color_transparent'] = $this->extracted->getTransparentColorFlag();
            if ($this->extracted->getTransparentColorFlag()) {
                $index = $this->extracted->getTransparentColorIndex();
                $this->frames[$i]['color_index'] = $index;
            }

            if ($this->frames[$i]['linked_disposal'] !== smartimagesImageGifExtractedInterface::DISPOSAL_METHOD_NONE) {
                $this->background_frames[$i] = $i;
            }

            $this->frames[$i]['frame_image_content'] = $this->extracted->getImageData();
            $this->frames[$i]['frame_image'] = $builder->getContents();


        }


        $finish = microtime(true);

        $delta = $finish - $start;
        file_put_contents('test.txt', $delta . ' сек.');
    }

    protected function getImageContentFromResource($resource)
    {
        ob_start();
        imagegif($resource);
        $image = ob_get_contents();
        ob_end_clean();

        return $image;
    }

    public function getDebugFrame($delay = 0, $exit = true)
    {
        $frame = $this->getFrameIndex($delay);
        if (array_key_exists($frame, $this->frames)) {
            $this->prepareFrames($frame);
            $frame = $this->frames[$frame];
            echo '<img src="data:image/gif;base64,' . base64_encode($this->getImageContentFromResource($frame['image_resource'])) . '" />';
            echo '<img src="data:image/gif;base64,' . base64_encode($frame['frame_image']) . '" />';
            if ($exit) {
                exit;
            }

            return;
        }
        throw new \Exception('Нет такогшо кадра');
    }

    public function getDebugImages()
    {
        echo '<html><body style="background-color: #144019; color:#fff;">';

        for ($i = 0; $i < 80; $i += 4) {

            echo $this->getBase64Image($i) . $this->getBase64FrameImage($i) . '<br><hr>';
        }

        echo '</body></html>';
        exit;

    }

    public function getDebugImage($dispose = 0, $delay = 4)
    {
        header('Content-type:image/gif');
        $this->prepareFrames(1);
        $builder = new smartimagesImageGifBuilderPlus();
        $builder->loop(55);
        $builder->setResponseType(smartimagesImageGifBuilderPlus::RESPONSE_PRINT);
        $builder->canvasWidth($this->frames[0]['width']);
        $builder->canvasWidth($this->frames[0]['height']);
        ob_end_clean();
        foreach ($this->frames as $k => $v) {
            $this->prepareFrames($k);
            $v = $this->getFrame($k);

            if ($dispose == 0) {
                $builder->addFrameFromImageContent($this->getImageContentFromResource($this->getImageResourceFromId($k)));
                $builder->delayTime($delay);
            } else {
                $builder->addFrame();
                if ($v['color_transparent']) {
                    $builder->transparentColorIndex($v['color_index']);
                    //$v['disposal'] = 3;
                } else {

                }
                $builder
                    ->imageWidth($v['width'])
                    ->imageHeight($v['height'])
                    ->imageLeft($v['left'])
                    ->imageTop($v['top'])
                    ->dispose($v['disposal'])
                    ->delayTime($v['delay'])
                    ->interlaceFlag($v['interlace'])
                    ->colorTable($v['color_table'])
                    ->imageData($v['frame_image_content']);


            }
        }
        $builder->addFooter();


        exit;
    }

    public function getDebugFrames()
    {
        echo '<html><body style="background-color: #000; color:#fff;">';

        foreach (array_keys($this->frames) as $k) {
            $frame = $this->getFrame($k);
            unset($frame['image']);
            unset($frame['frame_image']);
            unset($frame['frame_image_content']);
            //unset($frame['color_table']);
            echo '<pre>' . var_export($frame, true) . '</pre><br><hr>';
            waLog::dump($frame);
        }

        echo '</body></html>';
        exit;

    }
}