<?php

interface smartimagesImageGifExtractedInterface
{
    const DISPOSAL_METHOD_NONE = 1;
    const DISPOSAL_METHOD_BACKGROUND = 2;
    const DISPOSAL_METHOD_PREVIOUS = 3;

    /**
     * @return bool
     */
    public function hasNetscapeExtension();

    /**
     * @return int
     */
    public function getCanvasWidth();

    /**
     * @return int
     */
    public function getCanvasHeight();

    /**
     * @return bool
     */
    public function getGlobalColorTableFlag();

    /**
     * @return int
     */
    public function getTotalGlobalColors();

    /**
     * @return int
     */
    public function getTotalLoops();

    /**
     * @return bool
     */
    public function isAnimated();

    public function getImageContent($frame = 0);

    public function getImageResource($frame = 0);

    /**
     * @return int
     */
    public function count();
}