<?php

class smartimagesImageGifBuilderPlus
{
    /* Тип отдачи контента
        print сразу печатает контент
        buffer собирает и потом отдает в методе getContents
     */
    const RESPONSE_PRINT = 'print';
    const RESPONSE_BUFFER = 'buffer';

    protected $response_type = 'buffer';
    /**
     * @var int
     */
    protected $canvasWidth;

    /**
     * @var int
     */
    protected $canvasHeight;

    /**
     * @var int
     */
    protected $loop = null;

    /**
     * @var int[]
     */
    protected $disposes = [];

    /**
     * @var (null|int)[]
     */
    protected $transparentColorIndexes = [];

    /**
     * @var int[]
     */
    protected $delayTimes = [];

    /**
     * @var int[]
     */
    protected $imageLefts = [];

    /**
     * @var int[]
     */
    protected $imageTops = [];

    /**
     * @var int[]
     */
    protected $imageWidths = [];

    /**
     * @var int[]
     */
    protected $imageHeights = [];

    /**
     * @var bool[]
     */
    protected $interlaceFlags = [];

    /**
     * @var string[]
     */
    protected $colorTables = [];

    /**
     * @var string[]
     */
    protected $imageDatas = [];

    /**
     * @var int
     */
    protected $fp;

    protected $extractor = null;

    public function __construct()
    {
        $this->extractor = new smartimagesImageGifExtractor();
    }

    /**
     * @param  int $width
     *
     * @return $this
     */
    public function canvasWidth($width)
    {
        $this->canvasWidth = $width;

        return $this;
    }

    /**
     * @param  int $height
     *
     * @return $this
     */
    public function canvasHeight($height)
    {
        $this->canvasHeight = $height;

        return $this;
    }

    /**
     * @param  int $loop
     *
     * @return $this
     */
    public function loop($loop)
    {
        $this->loop = $loop;

        return $this;
    }

    /**
     * @param  int $dispose
     *
     * @return $this
     */
    public function dispose($dispose)
    {
        $this->disposes[$this->fp] = $dispose;

        return $this;
    }

    /**
     * @param  int $index
     *
     * @return $this
     */
    public function transparentColorIndex($index)
    {
        $this->transparentColorIndexes[$this->fp] = $index;

        return $this;
    }

    /**
     * @param  int $time
     *
     * @return $this
     */
    public function delayTime($time)
    {
        $this->delayTimes[$this->fp] = $time;

        return $this;
    }

    /**
     * @param  int $left
     *
     * @return $this
     */
    public function imageLeft($left)
    {
        $this->imageLefts[$this->fp] = $left;

        return $this;
    }

    /**
     * @param  int $top
     *
     * @return $this
     */
    public function imageTop($top)
    {
        $this->imageTops[$this->fp] = $top;

        return $this;
    }

    /**
     * @param  int $width
     *
     * @return $this
     */
    public function imageWidth($width)
    {
        $this->imageWidths[$this->fp] = $width;

        return $this;
    }

    /**
     * @param  int $height
     *
     * @return $this
     */
    public function imageHeight($height)
    {
        $this->imageHeights[$this->fp] = $height;

        return $this;
    }

    /**
     * @param  bool $flag
     *
     * @return $this
     */
    public function interlaceFlag($flag)
    {
        $this->interlaceFlags[$this->fp] = $flag;

        return $this;
    }

    /**
     * @param  int $contents
     *
     * @return $this
     */
    public function colorTable($contents)
    {
        $this->colorTables[$this->fp] = $contents;

        return $this;
    }

    public function addFrameFromImageContent($image_content)
    {
        $this->addFrameFromExtracted($this->extractor->extractFromContents($image_content));
    }

    public function addFrameFromImageStream($filename)
    {
        $this->addFrameFromExtracted($this->extractor->extractFromStream($filename));
    }

    public function addFrameFromFrame(smartimagesImageFrameGif $frame)
    {
        $content = $frame->getContent();
        $this->addFrameFromExtracted($this->extractor->extractFromContents($content));
        $tmpExtracted = $this->extractor->extractFromContents($content);
        $this->delayTime($frame->getDelay());
        $this->dispose($frame->getDisposal());
        if ($tmpExtracted->getTransparentColorFlag()) {
            $this->transparentColorIndex($tmpExtracted->getTransparentColorIndex());
        }
    }

    public function addFrameFromExtracted(smartimagesImageGifExtracted $extracted)
    {
        if (!isset($this->fp)) {
            $this
                ->canvasWidth($extracted->getCanvasWidth())
                ->canvasHeight($extracted->getCanvasHeight());
            if ($this->loop == null) {
                $this->loop($extracted->getTotalLoops());
            }
        }
        $this->addFrame();
        $this
            ->imageWidth($extracted->getImageWidth())
            ->imageHeight($extracted->getImageHeight())
            ->imageLeft($extracted->getImageLeft())
            ->imageTop($extracted->getImageTop())
            ->dispose($extracted->getDisposalMethod())
            ->delayTime($extracted->getDelayTime())
            ->interlaceFlag($extracted->getInterlaceFlag())
            ->colorTable($extracted->getColorTable())
            ->imageData($extracted->getImageData());
        if ($extracted->getTransparentColorFlag()) {
            $this->transparentColorIndex($extracted->getTransparentColorIndex());
        }
    }

    /**
     * @param  int $contents
     *
     * @return $this
     */
    public function imageData($contents)
    {
        $this->imageDatas[$this->fp] = $contents;

        return $this;
    }

    public function setResponseType($type = 'stream')
    {
        if ($type == self::RESPONSE_PRINT || $type == self::RESPONSE_BUFFER) {
            $this->response_type = $type;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addFrame()
    {
        if (!isset($this->fp)) {
            $this->fp = 0;
        } else {
            $this->fp++;
        }
        if ($this->fp > 0 && $this->response_type == self::RESPONSE_PRINT) {
            $prev_frame = $this->fp - 1;
            $this->printFrame($prev_frame);
        }

        return $this;
    }

    /**
     * @return string
     */
    protected function getHeaders()
    {
        // Header
        $contents = 'GIF89a';

        // Logical screen descriptor
        $contents .= pack('v*', $this->canvasWidth, $this->canvasHeight) . "\x00\x00\x00";

        // Netscape Extenstion
        $contents .= "\x21\xFF\x0B";
        $contents .= 'NETSCAPE2.0';
        $contents .= "\x03\x01";
        $contents .= pack('v', $this->loop);
        $contents .= "\x00";

        return $contents;
    }

    public function printFrame($number = 0)
    {
        if (array_key_exists($number, $this->imageDatas)) {
            if ($number == 0) {
                echo $this->getHeaders();
            }
            // Graphic control extension
            echo "\x21\xF9\x04";
            $unpack = $this->disposes[$number] << 2;
            if (isset($this->transparentColorIndexes[$number])) {
                $unpack = $unpack | 0b00000001;
                $index = $this->transparentColorIndexes[$number];
            } else {
                $index = null;
            }
            echo pack('C', $unpack);
            echo pack('v', $this->delayTimes[$number]);
            echo pack('C', $index);
            echo "\x00";

            // Image descriptor
            echo "\x2C";
            echo pack('v*', $this->imageLefts[$number], $this->imageTops[$number]);
            echo pack('v*', $this->imageWidths[$number], $this->imageHeights[$number]);
            $interlace = $this->interlaceFlags[$number] ? 0b01000000 : 0;
            $colorTableSize = log(strlen($this->colorTables[$number]) / 3, 2) - 1;
            $unpack = 0b10000000 | $interlace | $colorTableSize;
            $pack = pack('C', $unpack);
            echo $pack;

            // Local color table
            echo $this->colorTables[$number];

            // Image Data 
            echo preg_replace('/;$/s', '', $this->imageDatas[$number]);
        }

    }

    public function getFrame($number = 0)
    {
        $contents = '';

        if (array_key_exists($number, $this->imageDatas)) {
            if ($number == 0) {
                $contents .= $this->getHeaders();
            }
            // Graphic control extension
            $contents .= "\x21\xF9\x04";
            $unpack = $this->disposes[$number] << 2;
            if (isset($this->transparentColorIndexes[$number])) {
                $unpack = $unpack | 0b00000001;
            }
            $contents .= pack('C', $unpack);
            $contents .= pack('v', $this->delayTimes[$number]);
            $contents .= pack('C', $this->transparentColorIndexes[$number]);
            $contents .= "\x00";

            // Image descriptor
            $contents .= "\x2C";
            $contents .= pack('v*', $this->imageLefts[$number], $this->imageTops[$number]);
            $contents .= pack('v*', $this->imageWidths[$number], $this->imageHeights[$number]);
            $interlace = $this->interlaceFlags[$number] ? 0b01000000 : 0;
            $colorTableSize = log(strlen($this->colorTables[$number]) / 3, 2) - 1;
            $unpack = 0b10000000 | $interlace | $colorTableSize;
            $pack = pack('C', $unpack);
            $contents .= $pack;

            // Local color table
            $contents .= $this->colorTables[$number];

            // Image Data
            $contents .= $this->imageDatas[$number];
        }

        return $contents;

    }

    public function getContents()
    {
        // Header
        $contents = $this->getHeaders();

        for ($i = 0; $i < count($this->imageDatas); $i++) {

            // Graphic control extension
            $contents .= "\x21\xF9\x04";
            $unpack = $this->disposes[$i] << 2;
            if (isset($this->transparentColorIndexes[$i])) {
                $unpack = $unpack | 0b00000001;
            }
            $contents .= pack('C', $unpack);
            $contents .= pack('v', $this->delayTimes[$i]);
            $contents .= pack('C', $this->transparentColorIndexes[$i]);
            $contents .= "\x00";

            // Image descriptor
            $contents .= "\x2C";
            $contents .= pack('v*', $this->imageLefts[$i], $this->imageTops[$i]);
            $contents .= pack('v*', $this->imageWidths[$i], $this->imageHeights[$i]);
            $interlace = $this->interlaceFlags[$i] ? 0b01000000 : 0;
            $colorTableSize = log(strlen($this->colorTables[$i]) / 3, 2) - 1;
            $unpack = 0b10000000 | $interlace | $colorTableSize;
            $pack = pack('C', $unpack);
            $contents .= $pack;

            // Local color table
            $contents .= $this->colorTables[$i];

            // Image Data
            $contents .= $this->imageDatas[$i];
        }

        // Terminator
        $contents .= "\x3B";

        return $contents;
    }

    public function addFooter()
    {
        if ($this->response_type == self::RESPONSE_PRINT) {
            echo "\x3B";
        }
    }
}