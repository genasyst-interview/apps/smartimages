<?php

/* IMAGE OBJ */

abstract class smartimagesImage
{
    protected $layers = array();
    protected $quality = true;
    protected $init_quality = false;
    protected $frame_builder = null;
    protected $data = array();

    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function get($name)
    {
        return $this->data[$name];
    }

    public function has($name)
    {
        return array_key_exists($name, $this->data);
    }

    public function remove($name)
    {
        if ($this->has($name)) {
            unset($this->data[$name]);
        }
    }

    public function addFrame($data = array())
    {
        return $this;
    }

    /**
     * @return BackgroundLayer
     * @api
     */
    public function addBackgroundLayer()
    {
        return $this->layers[0] = new smartimagesImageLayerBackground($this);
    }

    public function setQuality($bool = true)
    {
        $this->quality = false;
        $this->init_quality = true;
    }

    public function getQuality()
    {
        return (bool)$this->quality;
    }

    public function isInitQuality()
    {
        return (bool)$this->init_quality;
    }

    /**
     * @return ImageLayer
     * @api
     */
    public function addImageLayer()
    {
        return new smartimagesImageLayerImage($this);
    }

    abstract public function getFrameBuilder();

    /**
     * @return smartimagesImageLayerText
     * @api
     */
    public function addTextLayer()
    {
        return new smartimagesImageLayerText($this);
    }

    public function setLayer(smartimagesImageLayerAbstract $layer, $position = false)
    {
        if ($position !== false) {
            $this->layers[(int)$position] = $layer;
        } else {
            $this->layers[] = $layer;
        }

        return $this;
    }

    public function getLayers()
    {
        return $this->layers;
    }

    const JPG = 'jpg';
    const PNG = 'png';
    const GIF = 'gif';

    const STREAM_TYPE_BUFFER = 'buffer';
    const STREAM_TYPE_PRINT = 'print';


    public function __construct($data = array())
    {
        $this->data = $data;
    }

    public function setData($key, $data)
    {
        $this->data[$key] = $data;
    }

    public function getData($key)
    {
        return array_key_exists($key, $this->data) ? $this->data[$key] : null;
    }

    public static function factory($type)
    {
        $class = 'smartimagesimage' . ucfirst(strtolower($type));

        return new $class();
    }

    abstract protected function buildImage($type);

    abstract protected function render();

    public function getCanvasWidth()
    {
        return intval($this->getData('width'));
    }

    public function getCanvasHeight()
    {
        return intval($this->getData('height'));
    }

    public function setCanvasWidth($int)
    {
        $this->setData('width', (int)$int);
    }

    public function setCanvasHeight($int)
    {
        $this->setData('height', (int)$int);
    }

    public function getImage()
    {
        return $this;
    }

    public function printContent()
    {
        header('Content-type:image/gif');
        $this->buildImage(static::STREAM_TYPE_PRINT);
    }

    public function getContent()
    {
        return $this->buildImage(static::STREAM_TYPE_BUFFER);
    }

    public function getBase64Content()
    {
        return base64_encode($this->getContent());
    }

    public function getBase64Image()
    {
        return '<img src="data:image/' . static::TYPE . ';base64,' . $this->getBase64Content() . '" />';
    }

}