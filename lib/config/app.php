<?php

return array(
    'name'       => 'smartimages', // _w('Smartgif')
    'icon'       => array(
        96 => 'img/smartimages96.png',
        48 => 'img/smartimages.png',
        24 => 'img/smartimages24.png',
        16 => 'img/smartimages16.png',
    ),
    'sash_color' => '#49a2e0',
    'frontend'   => true,
    'version'    => '2.2.14',
    'critical'   => '2.2.10',
    'vendor'     => 'webasyst',
    'system'     => true,
    'rights'     => true,
    'plugins'    => true,
    'themes'     => true,
    'auth'       => true,
    'csrf'       => true,
    'my_account' => true,
);
