<?php

return array(
    'login/'                => 'login/',
    'forgotpassword/'       => 'forgotpassword/',
    'signup/'               => 'signup/',
    'data/regions/'         => 'frontend/regions',
    'my/'                   => array(
        'module' => 'frontend',
        'action' => 'my',
        'secure' => true,
    ),
    'image/add/'            => 'frontend/imageAdd',
    'image/<id>/edit/'      => 'frontend/imageEdit',
    'category/add/'         => 'frontend/categoryAdd',
    'category/'             => 'frontend/categoryImages',
    'category/<id>/'        => 'frontend/categoryImages',
    'test/<method>/<data>/' => 'frontend/test',

    'test/<method>/' => 'frontend/test',
    '<url>/preview/' => 'frontend/preview',
    '<url>'          => 'frontend/',
);