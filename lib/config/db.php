<?php
return array(
    'smartimages_image'       => array(
        'id'             => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'timer_datetime' => array('datetime', 'null' => 0),

        'time_zone' => array('varchar', 255, 'null' => 0, 'default' => 'Europe/Moscow'),

        'timer_destroy' => array('int', 1, 'null' => 0, 'default' => '1'),
        'reverse'       => array('int', 1, 'null' => 0, 'default' => '1'),

        'create_datetime' => array('datetime', 'null' => 0),
        'data'            => array('text', 'null' => 0),
        ':keys'           => array(
            'PRIMARY' => 'id',
        ),
        ':options'        => array('engine' => 'MyISAM'),
    ),
    'smartimages_image_files' => array(
        'id'       => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'image_id' => array('int', 11, 'null' => 0),


        'file'     => array('text', 'null' => 0),
        ':keys'    => array(
            'PRIMARY' => 'id',
        ),
        ':options' => array('engine' => 'MyISAM'),
    ),
);