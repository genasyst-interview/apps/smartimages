<?php

class smartimagesForgotpasswordAction extends waForgotPasswordAction
{
    public function execute()
    {
        $this->setLayout(new smartimagesFrontendLayout());
        $this->setThemeTemplate('forgotpassword.html');
        parent::execute();
    }
}