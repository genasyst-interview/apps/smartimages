<?php

class smartimagesFrontendCategoryImagesAction extends waViewAction
{
    public function execute()
    {
        $this->setLayout(new smartimagesFrontendLayout());
        $contact = wa()->getUser()->getId();
        if ($contact) {
            $category_model = new smartimagesCategoryImagesModel();
            $image_model = new smartimagesImageModel();
            $category_id = waRequest::param('id');

            $categories = $category_model->getByContact($contact);
            foreach ($categories as &$v) {
                $v['url'] = wa()->getRouteUrl('smartimages/frontend/categoryImages', array('id' => $v['id']));
            }
            $this->view->assign('categories', $categories);
            if (waRequest::param('id') && array_key_exists($category_id, $categories)) {
                $images = $image_model->getByCategory($category_id, $contact);
                $this->view->assign('category', $categories[$category_id]);
            } else {
                $this->view->assign('category', array('id' => 0, 'name' => 'Все картинки'));
                $images = $image_model->getByContact($contact);
            }

            $this->view->assign('images', $images);
            $this->setThemeTemplate('category.html');
        }

    }

    public function printImage($data)
    {

    }

    protected function compileFrames($layers = array())
    {

    }

}
