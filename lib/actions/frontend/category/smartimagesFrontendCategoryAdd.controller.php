<?php

class smartimagesFrontendCategoryAddController extends waJsonController
{
    public function execute()
    {

        $contact = wa()->getUser()->getId();
        if ($contact) {
            $name = waRequest::post('name');
            if (!empty($name)) {
                $model = new smartimagesCategoryImagesModel();
                $model->insert(array(
                    'contact_id' => $contact,
                    'name'       => $name,
                ));
            }
        } else {
            $this->errors[] = 'Для добавления надо авторизоваться!';
        }

    }

    public function printImage($data)
    {

    }

    protected function compileFrames($layers = array())
    {

    }

}
