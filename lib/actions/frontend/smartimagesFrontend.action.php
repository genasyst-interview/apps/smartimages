<?php

class smartimagesFrontendAction extends waViewAction
{
    public function execute()
    {
        if (waRequest::method() == 'post') {
            if (waRequest::issetPost('new_image')) {
                $this->view->assign('image', array());
                $this->setThemeTemplate('countdown_form.html');
            }
        } else {
            if (waRequest::isXMLHttpRequest()) {

            } else {
                $this->setLayout(new smartimagesFrontendLayout());
                $this->getResponse()->setTitle('Создание счетчика');
                // $this->view->assign('breadcrumbs',);
                $this->setThemeTemplate('countdown.html');
            }
        }
    }


    public function display($clear_assign = true)
    {
        return parent::display(false);
    }

}
