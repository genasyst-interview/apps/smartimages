<?php

class smartimagesFrontendController extends waJsonController
{
    public function execute()
    {
        $data = waRequest::post();
        $image = smartimagesImage::factory('gif');

        $image->setData($data);
        $image->getBase64Content();
    }
}