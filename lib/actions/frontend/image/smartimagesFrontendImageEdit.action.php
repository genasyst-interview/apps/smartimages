<?php

class smartimagesFrontendImageEditAction extends waViewAction
{
    public function execute()
    {
        $id = waRequest::param('id');
        $model = new smartimagesImageModel();

        $image = $model->getById($id);
        if (!empty($image) && wa()->getUser()->getId() == $image['contact_id']) {
            $this->view->assign('image', $image);
            if (unserialize($image['data'])) {
                $layers = unserialize($image['data']);
                foreach ($layers as $k => $v) {
                    $layers[$k]['id'] = $k;
                }
                $this->view->assign('layers', $layers);
            } else {
                $this->view->assign('layers', array());
            }
        } else {

        }
    }


    public function display($clear_assign = true)
    {
        return parent::display(false);
    }
}
