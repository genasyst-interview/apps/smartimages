<?php

class smartimagesFrontendImageAddController extends waJsonController
{
    public function execute()
    {
        if (waRequest::method() == 'post' && !empty(wa()->getUser()->getId())) {
            $data = array(
                'name'        => waRequest::post('name'),
                'filename'    => waRequest::post('filename'),
                'type'        => waRequest::post('type'),
                'data'        => serialize(array()),
                'category_id' => waRequest::post('category_id'),
                'contact_id'  => wa()->getUser()->getId(),
            );
            $model = new smartimagesImageModel();
            $model->add($data);
        } else {
            if (waRequest::isXMLHttpRequest()) {

            } else {
                $this->setLayout(new smartimagesFrontendLayout());
                $this->getResponse()->setTitle('Создание счетчика');
                // $this->view->assign('breadcrumbs',);
                $this->setThemeTemplate('countdown.html');
            }
        }
    }

}
