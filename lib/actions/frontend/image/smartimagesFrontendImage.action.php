<?php

class smartimagesFrontendImageAction extends waAction
{
    public function execute()
    {
        $filepath = waRequest::server('REQUEST_URI');
        $image_model = new smartimagesImageModel();
        $image = $image_model->getByPath($filepath);
        if (!$image && waRequest::param('id', false)) {
            $image = $image_model->getById(waRequest::param('id', 0, waRequest::TYPE_INT));
        }
        if ($image) {
            if (unserialize($image['data'])) {
                $layers = unserialize($image['data']);
            } else {
                $layers = array();
            }


        } else {
            // 404
        }

    }

    public function printImage($data)
    {
        if (unserialize($data['data'])) {
            $layers = unserialize($data['data']);
        } else {
            $layers = array();
        }
    }

    protected function compileFrames($layers = array())
    {
        $frames = array();
        foreach ($layers as $layer) {
            $start = $layer['start'];
            $type = $layer['type'];
            if ($type == 'image') {

            } elseif ($type == 'text') {

            }
            $frames[$start][] = $layer;
        }
    }

}
