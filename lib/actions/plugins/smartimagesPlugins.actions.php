<?php

class smartimagesPluginsActions extends waPluginsActions
{
    public function preExecute()
    {
        if (!$this->getUser()->isAdmin('smartimages')) {
            throw new waRightsException(_ws('Access denied'));
        }
    }
}
