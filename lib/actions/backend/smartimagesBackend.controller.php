<?php

class smartimagesBackendController extends waViewController
{
    public function execute()
    {
        if (($d = waRequest::get('domain_id')) && $d != $this->getUser()->getSettings('smartimages', 'last_domain_id')) {
            $this->getUser()->setSettings('smartimages', 'last_domain_id', $d);
        }
        $this->setLayout(new smartimagesDefaultLayout());
    }
}
