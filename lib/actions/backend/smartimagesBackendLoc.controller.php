<?php

/**
 * A list of localized strings to use in JS.
 */
class smartimagesBackendLocController extends waViewController
{
    public function execute()
    {
        $this->executeAction(new smartimagesBackendLocAction());
    }

    public function preExecute()
    {
        // do not save this page as last vismartimagesd
    }
}
