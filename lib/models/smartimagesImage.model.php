<?php

class smartimagesImageModel extends waModel
{
    protected $table = 'smartimages_image';

    public function getByPath($path = '')
    {
        return $this->getByField(array('path' => $path));
    }

    public function getByCategory($category_id, $contact_id)
    {
        return $this->getByField(array('category_id' => $category_id, 'contact_id' => $contact_id), 'id');
    }

    public function getByContact($contact_id)
    {
        return $this->getByField(array('contact_id' => $contact_id), 'id');
    }

    public function add($data = array())
    {
        $data['create_datetime'] = $data['edit_datetime'] = date("Y-m-d H:i:s");

        return $this->insert($data);
    }
}