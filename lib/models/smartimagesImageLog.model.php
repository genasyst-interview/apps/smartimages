<?php

class smartimagesImageLogModel extends waModel
{
    protected $table = 'smartimages_image_log';
    CONST CRYPT_KEY = 'smrtimg';
    CONST COOKIE_KEY = '_smrtimg';

    protected function encodeId($id)
    {
        return $this->encrypt($id, self::CRYPT_KEY);
    }

    protected function decodeId($id)
    {
        return $this->decrypt($id, self::CRYPT_KEY);
    }

    protected function encrypt($decrypted, $key)
    {
        $ekey = hash('SHA256', $key, true);
        srand();
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
        if (strlen($iv_base64 = rtrim(base64_encode($iv), '=')) != 22) return false;
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $ekey, $decrypted . md5($decrypted), MCRYPT_MODE_CBC, $iv));

        return $iv_base64 . $encrypted;
    }

    protected function decrypt($encrypted, $key)
    {
        $ekey = hash('SHA256', $key, true);
        $iv = base64_decode(substr($encrypted, 0, 22) . '==');
        $encrypted = substr($encrypted, 22);
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $ekey, base64_decode($encrypted), MCRYPT_MODE_CBC, $iv), "\0\4");
        $hash = substr($decrypted, -32);
        $decrypted = substr($decrypted, 0, -32);
        if (md5($decrypted) != $hash) return false;

        return $decrypted;
    }

    protected function getContact()
    {
        wa('smartimages');
        $contact = false;
        if (wa()->getUser()->isAuth()) {
            $contact = wa()->getUser();
        } elseif (!empty(waRequest::cookie(self::COOKIE_KEY))) {
            $id = trim($this->decodeId(waRequest::cookie(self::COOKIE_KEY)));
            if (!empty(intval($id))) {
                $contact_model = new waContactModel();
                $contact_data = $contact_model->getById((int)$id);
                try {
                    $contact = new waContact($contact_data);
                } catch (waException $e) {

                }
            }
        }
        if (!$contact) {
            $contact = new waContact();
        }
        $domain = false;
        if (array_key_exists('HTTP_REFERER', $_SERVER)) {
            if (preg_match('@/installer/@', $_SERVER['HTTP_REFERER'])) {
                $domain = self::cutDomain($_SERVER['HTTP_REFERER']);
            }
        }
        if (empty($contact->getId())) {
            if ($domain) {
                $contact->set('url', $domain);
                $contact->set('name', $domain);
            } else {
                $contact->set('name', 'client_' . time());
            }
            $contact->save();
            $contact->addToCategory('smartimages');
            $id = $contact->getId();
            if ($id > 0) {
                wa()->getResponse()->setCookie(self::COOKIE_KEY,
                    $this->encodeId($id),
                    time() + 100000000, '/', self::cutDomain(wa()->getRouting()->getDomain(null, true))
                );
            }
        } else {
            if ($domain) {
                if (preg_match('/client_\d+/', $contact->getName())) {
                    $contact->set('name', $domain);
                }
                $url_add = true;
                $urls = $contact->get('url');
                if (is_array($urls)) {
                    foreach ($urls as $url) {
                        if (self::cutDomain($url['value']) == $domain) {
                            $url_add = false;
                        }
                    }
                }
                if ($url_add) {
                    $contact->set('url.' . time(), $domain);
                    $contact->save();
                }
            }
        }

        return $contact;
    }

    public function add($image_id, $data = null, $contact_id = null, $errors = '')
    {
        $fields = $this->getMetadata();
        if ($data === null) {
            $data = array();
            foreach ($fields as $name => $v) {
                $server_name = strtoupper($name);
                if (array_key_exists($server_name, $_SERVER)) {
                    $data[$name] = (string)$_SERVER[$server_name];
                }
            }
            $data['referer_domain'] = null;
            if (array_key_exists('HTTP_REFERER', $_SERVER)) {
                $data['referer_domain'] = self::cutDomain($_SERVER['HTTP_REFERER']);
            }
            if (array_key_exists('REQUEST_URI', $_SERVER) && !empty($_SERVER['REQUEST_URI'])) {
                $data['filename'] = basename($_SERVER['REQUEST_URI']);
            } elseif (array_key_exists('SCRIPT_URI', $_SERVER) && !empty($_SERVER['SCRIPT_URI'])) {
                $data['filename'] = basename($_SERVER['SCRIPT_URI']);
            } elseif (array_key_exists('SCRIPT_URL', $_SERVER) && !empty($_SERVER['SCRIPT_URL'])) {
                $data['filename'] = basename($_SERVER['SCRIPT_URL']);
            }

            if (array_key_exists('REQUEST_TIME', $_SERVER)) {
                $data['request_datetime'] = date("Y-m-d H:i:s", intval($_SERVER['REQUEST_TIME']));
            }
            $data['datetime'] = date("Y-m-d H:i:s", time());
        } else {
            $data_temp = array();
            foreach ($data as $k => $v) {
                $data_temp[strtolower($k)] = $v;
            }
            /// ДОМЕНЗ АПРОСИВШЕГО
            if (!array_key_exists('referer_domain', $data_temp)) {
                $data_temp['referer_domain'] = null;
                if (array_key_exists('http_referer', $data_temp)) {
                    $data_temp['referer_domain'] = self::cutDomain($data_temp['http_referer']);
                }
            }
            ///  ФАЙЛ
            if (array_key_exists('request_uri', $data_temp) && !empty($data_temp['request_uri'])) {
                $data_temp['filename'] = basename($data_temp['request_uri']);
            } elseif (array_key_exists('script_uri', $data_temp) && !empty($data_temp['script_uri'])) {
                $data_temp['filename'] = basename($data_temp['script_uri']);
            } elseif (array_key_exists('script_url', $data_temp) && !empty($data_temp['script_url'])) {
                $data_temp['filename'] = basename($data_temp['script_url']);
            }
            /// Временные метки
            if (array_key_exists('request_time', $data_temp)) {
                $data_temp['request_datetime'] = date("Y-m-d H:i:s", intval($data_temp['request_time']));
            }
            if (!array_key_exists('datetime', $data_temp)) {
                $data_temp['datetime'] = $data_temp['request_datetime'];
            }
            $data = array();
            foreach ($data_temp as $k => $v) {
                if (array_key_exists($k, $fields)) {
                    $data[$k] = $v;
                }
            }
        }
        $data['errors'] = (string)$errors;
        $data['image_id'] = (int)$image_id;
        $data['contact_id'] = $contact_id;


        $this->insert($data);
    }

    public function prepare($image_id, $data = null, $errors = '')
    {
        $fields = $this->getMetadata();
        if ($data === null) {
            $data = array();
            foreach ($fields as $name => $v) {
                $server_name = strtoupper($name);
                if (array_key_exists($server_name, $_SERVER)) {
                    $data[$name] = (string)$_SERVER[$server_name];
                }
            }
            $data['referer_domain'] = null;
            if (array_key_exists('HTTP_REFERER', $_SERVER)) {
                $data['referer_domain'] = self::cutDomain($_SERVER['HTTP_REFERER']);
            }

            if (array_key_exists('REQUEST_URI', $_SERVER) && !empty($_SERVER['REQUEST_URI'])) {
                $data['filename'] = basename($_SERVER['REQUEST_URI']);
            } elseif (array_key_exists('SCRIPT_URI', $_SERVER) && !empty($_SERVER['SCRIPT_URI'])) {
                $data['filename'] = basename($_SERVER['SCRIPT_URI']);
            } elseif (array_key_exists('SCRIPT_URL', $_SERVER) && !empty($_SERVER['SCRIPT_URL'])) {
                $data['filename'] = basename($_SERVER['SCRIPT_URL']);
            }

            if (array_key_exists('REQUEST_TIME', $_SERVER)) {
                $data['request_datetime'] = date("Y-m-d H:i:s", intval($_SERVER['REQUEST_TIME']));
            }
            $data['datetime'] = date("Y-m-d H:i:s", time());
        } else {
            $data_temp = array();
            foreach ($data as $k => $v) {
                $data_temp[strtolower($k)] = $v;
            }
            /// ДОМЕНЗ АПРОСИВШЕГО
            if (!array_key_exists('referer_domain', $data_temp)) {
                $data_temp['referer_domain'] = null;
                if (array_key_exists('http_referer', $data_temp)) {
                    $data_temp['referer_domain'] = self::cutDomain($data_temp['http_referer']);
                }
            }
            ///  ФАЙЛ
            if (array_key_exists('request_uri', $data_temp) && !empty($data_temp['request_uri'])) {
                $data_temp['filename'] = basename($data_temp['request_uri']);
            } elseif (array_key_exists('script_uri', $data_temp) && !empty($data_temp['script_uri'])) {
                $data_temp['filename'] = basename($data_temp['script_uri']);
            } elseif (array_key_exists('script_url', $data_temp) && !empty($data_temp['script_url'])) {
                $data_temp['filename'] = basename($data_temp['script_url']);
            }
            /// Временные метки
            if (array_key_exists('request_time', $data_temp)) {
                $data_temp['request_datetime'] = date("Y-m-d H:i:s", intval($data_temp['request_time']));
            }
            if (!array_key_exists('datetime', $data_temp)) {
                $data_temp['datetime'] = $data_temp['request_datetime'];
            }
            $data = array();
            foreach ($data_temp as $k => $v) {
                if (array_key_exists($k, $fields)) {
                    $data[$k] = $v;
                }
            }
        }
        $data['errors'] = (string)$errors;
        $data['image_id'] = (int)$image_id;


        return $data;
    }

    public static function cutDomain($url)
    {
        preg_match('@^(?:http://|https://)?([^/]+)([\/].*)?@i', mb_strtolower($url), $host_arr);

        $name = $host_arr[1];
        if (preg_match('/^www\.(.+)\..+/', $name)) {
            $name = preg_replace('/^www\./i', '', $name);
        }

        return trim(strtolower($name));
    }

}