<?php

class smartimagesCategoryImagesModel extends waModel
{
    protected $table = 'smartimages_category_images';

    public function getByContact($id)
    {
        return $this->getByField('contact_id', $id, 'id');
    }
}